const { defineConfig } = require('@vue/cli-service')

module.exports = defineConfig({
  publicPath: "/metaboserv",
  outputDir: "dist/metaboserv/",
  transpileDependencies: true,
  lintOnSave: false
})
