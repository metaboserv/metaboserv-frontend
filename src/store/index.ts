import { createStore } from 'vuex';
import Cookies from 'js-cookie';

export interface customObject {
    [key: string]: any
};

const store = createStore({
    state: {
        /**
         * Denotes all (client-side) store attributes. Most of them are removed if the tab is refreshed or closed.
         * Persistent attributes (for now only bearer tokens) are stored as cookies.
         * 
         * -- Meta Data and Non-User Query Data --
         * studies_all - Contains names of all studies visible to the current user (if not logged in, only public ones)
         * studies_selected - Contains only selected studies (toggled with checkboxes)
         * metabolites_all - Names and metadata for all metabolites
         * metabolites_order_map - Stores the calculated "ranks" of all metabolites in the list for ordering purposes
         * metabolites_selected - Contains only metabolites that are selected (initially empty)
         * metabolites_unselected - Contains only unselected metabolites (initially all)
         * qid - Gives an identifier to current query
         * query_active_queries - Contains all metabolite queries that can be answered considering the current configuration
         * query_filters - The last used query filters (particularly for studies)
         * query_inactive_queries - Contains queries that are valid, but not currently possible (e.g. because a selected metabolite is not contained in a study)
         * query_name_id_map - Maps metabolite names to their internal IDs (e.g. 3-Hydroxybutyric Acid to 3-hydroxybutyric_acid)
         * query_strictness_string - Either "default", "strict" or "custom". Denotes the strictness of the query
         * query_strictness_value - Numerical representation of strictness string
         * sliders - Holds percentage values of the interface conc. sliders (metabolite -[min,max])
         * access_tokens - Holds access tokens. These can provide specific permissions that exist outside of the normal user account system when provided.
         * 
         * -- Info Panel --
         * info_lock_state - If true, locks the metabolite/study info panel (it won't disappear or change content)
         * info_pos - Absolute position (in px) of the top side of the info panel
         * info_to_show - Content of the info panel
         * info_visible_state - If true, info panel is shown. Hidden if false
         * 
         * -- Current and stored query results --
         * concentrations_normal - Currently stored normal concentrations (metabolite - [min, max])
         * plot_image_urls - Holds the URLs of all fetched plots
         * query_nconc_string - Holds the normal conc. info in a parsable string (for HTTP queries)
         * query_results - The results of the current user query
         * query_results_mode - Full or partial query
         * query_results_in_storage - If true, current query results are saved in the "persistent storage" (persistent until refresh)
         * query_results_metabolites - A list of metabolite names used for the current query
         * stored_metabolites - Saves the metabolites used for any stored query
         * stored_results - Holds query results ("persistent storage") in a map, accessible by query identifier
         * temp_results - Holds results for internal calculations that are never shown to the user (e.g. when calculating a subset)
         * 
         * -- Misc --
         * download_selected_item - Identifier of the subset to download (used in /download path)
         * metabolites_comparator - A comparator for ordering the metabolite list by name and category
         * misc_last_view_path - Either /graph, /table or /raw. Denotes the last visited of these three
         * query_loading - If true, query is in progress
         * 
         */
        access_tokens: [] as Array<string>,
        concentrations_normal: new Map<String, [Number, Number]>() as Map<String, [Number, Number]> | any,
        download_selected_item: "" as String,
        info_lock_state: false as Boolean,
        info_visible_state: false as Boolean,
        info_pos: 90 as Number,
        info_to_show: {} as customObject,
        metabolites_all: [] as Array<customObject>,
        metabolites_comparator: function(a: customObject, b:customObject){return -1;},
        metabolite_order_map: new Map<String, number>() as Map<String, number> | any,
        metabolites_selected: [] as Array<customObject>,
        metabolites_unselected: [] as Array<customObject>,
        misc_last_view_path: "/graph" as String,
        plot_image_urls: [] as Array<[string, string]>,
        selected_phenotypes: [] as Array<string>,
        phenotype_metadata: new Map<string, customObject>() as Map<string, customObject>,
        sliders: new Map<String, Array<Number>>() as Map<String, Array<Number>>,
        studies_all: [] as Array<String>,
        studies_selected: [] as Array<String>,
        studies_metadata: new Map<String, customObject>() as Map<String, customObject> | any,
        query_filters: new Map<String, String | Array<Number>>() as Map<String, String | Array<Number>> | any,
        query_results: [] as customObject,
        query_results_metabolites: [] as Array<[String, String]>,
        query_results_phenotypes: [] as Array<String>,
        query_results_in_storage: false as Boolean,
        query_strictness_string: "strict" as String,
        query_strictness_value: -1 as number,
        query_name_id_map: new Map<String, String>() as Map<String, String> | any,
        query_active_queries: new Map<String,String>() as Map<String, String> | any,
        query_inactive_queries: new Map<String, String>() as Map<String, String> | any,
        query_nconc_string: "" as String,
        query_loading: false as Boolean,
        qid: "" as String,
        stored_results: new Map<String, customObject>() as Map<String, customObject> | any,
        stored_metabolites: new Map<String, customObject>() as Map<String, customObject> | any,
        temp_results: {} as customObject,


    } as customObject,
    mutations: {
        accessTokensAdd (state, token: string) {
            state.access_tokens.push(token);
        },
        accessTokensRemove (state, token: string) {
            const item_index = state.access_tokens.indexOf(token);
            if (item_index != -1) state.access_tokens.splice(item_index, 1);
        },
        accessTokensClear (state) {
            state.access_tokens.splice(0, state.access_tokens.length);
        },
        /* Caches a normal concentration range */
        concentrationsNormalCache (state, payload) {
            state.concentrations_normal.set(payload.name, payload.range);
        },
        /* Removes an entry from normal concentration map */
        concentrationsNormalRemove(state, id) {
            state.concentrations_normal.delete(id);
        },
        /* Clears the normal concentration map */
        concentrationsNormalClear (state) {
            state.concentrations_normal.clear();
        },
        /* Completely clears metabolite list, order map and selected/unselected lists */
        metabolitesClear (state) {
            state.metabolites_all = []
            state.metabolites_selected = []
            state.metabolites_unselected = []
            state.metabolite_order_map.clear();
        },
        /* Adds a metabolite to selection and removes it from unselected list */
        metabolitesSelectedAdd (state, metabolite: customObject) {
            if (state.metabolites_selected.map(function(x: customObject) { return x.metabolite_name}).indexOf(metabolite.metabolite_name) == -1) {
                state.metabolites_selected = state.metabolites_selected.concat(metabolite);
            }
            const uns_index = state.metabolites_unselected.map(function(x: customObject) { return x.metabolite_name}).indexOf(metabolite.metabolite_name)
            if (uns_index != -1) {
                state.metabolites_unselected.splice(<number>uns_index, 1);
            }
        },
        /* Removes a metabolite from selection and adds it to unselected list */
        metabolitesSelectedRemove (state, metabolite: customObject) {
            if (state.metabolites_unselected.map(function(x: customObject) { return x.metabolite_name}).indexOf(metabolite.metabolite_name) == -1 || !state.metabolites_unselected.length) {
                state.metabolites_unselected.push(metabolite);
            }
            const s_index = state.metabolites_selected.map(function(x: customObject) { return x.metabolite_name}).indexOf(metabolite.metabolite_name)
            if (s_index != -1) {
                state.metabolites_selected.splice(<number>s_index, 1);
                state.query_active_queries.delete(metabolite.id);
            }
        },
        /* Creates a metabolite comparator function for sorting/ordering the list */
        metabolitesCreateComparator(state) {
            state.metabolites_comparator = function(a: customObject, b: customObject) {
                if (!state.metabolite_order_map) {
                    state.metabolite_order_map = new Map<string, number>();
                }
                if (state.metabolite_order_map.get(a.id) > state.metabolite_order_map.get(b.id)) {return -1; } 
                else if (state.metabolite_order_map.get(a.id) < state.metabolite_order_map.get(b.id)) { return 1; }
                else {
                    if (a['metabolite_cat'] == "lipid" && b['metabolite_cat'] == "non-lipid") { return 1; }
                    else if (a['metabolite_cat'] == "non-lipid" && b['metabolite_cat'] == "lipid") { return -1; }
                    else { return a['metabolite_name'].localeCompare(b['metabolite_name']); }
                } 
            }
            
        },
        /** Calculates the "rank" of a metabolite in the list
        *   The rank is based on how many of the selected studies the metabolite is contained in
        */
        metabolitesUpdateOrderMap (state, metabolite: customObject) {
            let matched: number = 0;
            for (let study of metabolite.studies) {
                if (state.studies_selected.includes(study)) {
                        matched = matched + 1;
                }
            }
            state.metabolite_order_map.set(metabolite.id.replace(/["']/g, ""), matched);
        },
        /* Sets content for the info panel */
        infoModalSetContent (state, payload) {
            state.info_to_show = payload.content;
            state.info_pos = payload.y;
        },
        /* Makes the info panel visible */
        infoModalShow (state) {
            state.info_visible_state = true;
        },
        /* Hides the info panel */
        infoModalHide (state) {
            state.info_visible_state = false;
        },
        /* Locks the info panel (position, visibility and content) */
        infoModalLock (state) {
            state.info_lock_state = true;
        },
        /* Unlocks the info panel */
        infoModalUnlock (state) {
            state.info_lock_state = false;
        },
        /* Sets a viewpath to navigate to when user clicks on "Browse" */
        miscSetViewPath (state, path: string) {
            state.misc_last_view_path = path;
        },
        /* Adds metadata for a particular phenotype (e.g. AKI) into store */
        phenotypeMetadataAdd (state, payload) {
            state.phenotype_metadata.set(payload.phenotype_id, payload.metadata);
        },
        /* Adds a phenotype to query selection */
        phenotypeAdd (state, pid: string) {
            state.selected_phenotypes.push(pid);
            state.selected_phenotypes.sort();
        },
        /* Removes a phenotype from query selection */
        phenotypeRemove (state, pid: string) {
            if (state.selected_phenotypes.includes(pid))
                state.selected_phenotypes.splice(state.selected_phenotypes.indexOf(pid), 1);
            state.selected_phenotypes.sort();
        },
        /* Removes all phenotypes from query selection, also clears metadata */
        phenotypeClear (state) {
            state.selected_phenotypes.splice(0, state.selected_phenotypes.length);
            state.phenotype_metadata.clear();
        },
        /* Clears the study lists (all and selected) */
        studyClear (state) {
            state.studies_all = [];
            state.studies_selected = [];
            state.studies_metadata.clear();
        },
        /* Sorts the study arrays */
        studySort (state) { 
            state.studies_all.sort();
            state.studies_selected.sort();
        },
        /* Adds a study identifier to both lists */
        studyKnownAdd (state, study: string) {
            state.studies_all.push(study);
            state.studies_selected.push(study);
        },
        /* Adds a study to selection */
        studySelectedAdd (state, study: string) {
            state.studies_selected.push(study);
        },
        /* Removes a study from selection */
        studySelectedRemove (state, study: string) {
            const study_index = state.studies_selected.indexOf(study);
            state.studies_selected.splice(study_index, 1);
        },
        /* Adds study metadata (in object form) to the according map */
        studyAddMetadata (state, payload) {
            state.studies_metadata.set(payload.study_id, payload.metadata);
        },
        /* Sets study filters */
        queryStudyFiltersSet (state, query_filters: customObject) {
            if ('study_date' in query_filters) state.query_filters.set('study_date', query_filters.study_date);
            if ('study_type' in query_filters) state.query_filters.set('study_type', query_filters.study_type);
            if ('study_method' in query_filters) state.query_filters.set('study_method', query_filters.study_method);
            if ('study_authors' in query_filters) state.query_filters.set('study_authors', query_filters.study_authors);
            if ('visibility' in query_filters) state.query_filters.set('visibility', query_filters.visibility);
            if ('experiment_date' in query_filters) state.query_filters.set('experiment_date', query_filters.experiment_date);
        },
        /* Resets study filters */
        queryStudyFiltersReset (state) {
            state.query_filters.clear();
        },
        /* Sets query results */
        queryResultsSet (state, query_results: customObject) {
            state.query_results = query_results;
            state.query_results_in_storage = false;
        },
        /* Saves query metabolites */
        queryResultsSetMetadata (state, payload) {
            const query_normalized = payload.query_metabolites.filter((met: string[]|undefined) => met !== undefined);
            if (query_normalized.length > 0) state.query_results_metabolites = query_normalized;
        },
        /* Stores the phenotypes used in the last query (for plotting reasons)*/
        queryPhenotypesSet (state, phenotypes: string[]) {
            state.query_results_phenotypes = phenotypes;
        },
        /* Sets query mode (full or partial) */
        queryResultsSetMode (state, mode: string) {
            state.query_results_mode = mode;
        },
        /* Removes an active query */
        queryRemove (state, id: string) {
            if (state.query_active_queries.has(id)) {
                state.query_active_queries.delete(id);
            }
        },
        /* Sets a query to inactive status */
        querySetInactive (state, query: [string, string]) {
            state.query_inactive_queries.set(query[0], query[1]);
            if (state.query_active_queries.has(query[0])) {
                state.query_active_queries.delete(query[0]);
            }
        },
        /* Sets an inactive query to active status */
        querySetActive (state, query: [string, string]) {
            state.query_active_queries.set(query[0], query[1]);
            if (state.query_inactive_queries.has(query[0])) {
                state.query_inactive_queries.delete(query[0]);
            }
        },
        /* Stores the current query results in a map */
        queryStoredSet (state, payload) {
            state.stored_results.set(payload.qid, payload.content);
            if (payload.content == state.query_results) state.query_results_in_storage = true;
        },
        /* Deletes stored query results by a query identifier (qid) */
        queryStoredDelete (state, qid: string) {
            if (state.stored_results.get(qid) == state.query_results) state.query_results_in_storage = false;
            state.stored_results.delete(qid);
        },
        /* Calculates and sets strictness value based on the string */
        queryStrictnessSetString (state, strictness: string) {
            state.query_strictness_string = strictness;
            switch (strictness) {
                case "default": state.query_strictness_value = 1; break;
                case "strict": state.query_strictness_value = 0; break;
                default: break;
            }
        },
        /* Sets a custom strictness value (potentially different from 0 and 1) */
        queryStrictnessSetValueCustom (state, custom_strictness: number) {
            state.query_strictness_value = custom_strictness;
        },
        /* Sets the nconc string */
        queryNconcStringSet (state, nconc_string: string) {
            state.query_nconc_string = nconc_string;
        },
        /* Toggles whether a query is being processed or not */
        queryToggleLoading (state) {
            state.query_loading = !state.query_loading;
        },
        /* Sets temp results (internal) */
        queryTempSet (state, query_results: customObject) {
            state.temp_results = query_results;
        },
        /* Clears temp results */
        queryTempClear (state) {
            state.temp_results = {};
        },
        /* Stores metabolites associated with some stored query results */
        queryStoredMetabolitesSet (state, payload) {
            state.stored_metabolites.set(payload.qid, payload.metabolite_list);
        },
        /* Deletes metabolites associated with some particular stored query result (qid) */
        queryStoredMetabolitesDelete (state, qid: string) {
            state.stored_metabolites.delete(qid);
        },
        /* Sets current query identifier (qid) */
        queryIDSet (state, qid: string) {
            state.qid = qid;
        },
        /* Adds a plot URL (and type) to the plot list */
        plotAdd (state, payload) {
            state.plot_image_urls.push([payload.url, payload.plot_type]);
        },
        /* Replaces a plot in the list */
        plotReplace (state, payload) {
            state.plot_image_urls.splice(payload.plot_index, 1, [payload.url, payload.plot_type]);
        },
        /* Removes all currently stored plots */
        plotReset (state) {
            state.plot_image_urls = state.plot_image_urls.slice(state.plot_image_urls.length);
        },
        /* Stores identifier of what the user wants to download */
        downloadSelectedSet (state, item: string) {
            state.download_selected_item = item;
        },
        /* Resets a lot of variables on signout */
        signOutResetApp (state) {
            state.query_name_id_map.clear();
            state.query_results_in_storage = false;
            state.query_results = [];
            state.query_results_metabolites = [];
            state.concentrations_normal.clear();
            state.qid = "";
            state.query_active_queries.clear();
            state.query_inactive_queries.clear();
            state.studies_all = [];
            state.studies_selected = [];
        },
        interfaceCacheSlider (state, payload) {
            state.sliders.set(payload.metabolite, payload.range);
        },
        interfaceClearSlider (state) {
            state.sliders.clear();
        },
    },
    actions: {
        /* Creates comparator and puts metabolites into the unselected list (performed on startup) */
        metabolitesLoad ({state, commit}, metabolites: Array<customObject>) {
            commit('metabolitesCreateComparator');
            state.metabolites_all = metabolites;
            if (!state.metabolites_unselected.length && !state.metabolites_selected.length) {
                state.metabolites_unselected = [];
                metabolites.forEach((mt: customObject) => state.metabolites_unselected.push(Object.assign({}, mt))) //DeepCopy. Doesnt work otherwise!
            }
        },
        /* Sorts metabolite lists (selected and unselected) */
        metabolitesSort ({state}) {
            state.metabolites_selected.sort(state.metabolites_comparator);
            state.metabolites_unselected.sort(state.metabolites_comparator);
        },
        /* Calculates new rank(s) for all metabolites */
        metabolitesUpdateAll ({state, commit}) {
            state.metabolites_all.forEach((metabolite: customObject) => {
                commit('metabolitesUpdateOrderMap', metabolite);
            })
        },
        /* Creates a new query */
        queryCreate ({state, commit}, payload) {
            state.query_active_queries.set(payload.id, payload);
            if (state.metabolite_order_map.get(payload.id) == 0) { 
                commit('querySetInactive', ([payload.id, payload.query]));
            }       
            if (!state.query_name_id_map.get(payload.name)) {
                state.query_name_id_map.set(payload.name, payload.id);
            }
        },
        /* Adds highly ranked metabolites to selection */
        metabolitesSelectedAddBest ({state, commit}) {
            return new Promise(resolve => {
                let items_checked:number = state.metabolites_unselected.length;
                while(items_checked) {
                    if (state.metabolite_order_map.get(state.metabolites_unselected[items_checked-1].id) == state.studies_selected.length) {
                        commit('metabolitesSelectedAdd', state.metabolites_unselected[items_checked-1]);
                        items_checked--;
                    }
                    else {
                        items_checked--;
                    }
                }
                resolve('metabolitesSelectedAddBest resolved');
            })
        },
        /* Adds all metabolites to selection regardless of rank */
        metabolitesSelectedAddAll ({state, commit}) {
            return new Promise(resolve => {
                let items_checked:number = state.metabolites_unselected.length;
                while(items_checked) {
                    if (state.metabolite_order_map.get(state.metabolites_unselected[items_checked-1].id) > 0) {
                        commit('metabolitesSelectedAdd', state.metabolites_unselected[items_checked-1]);
                        items_checked--;
                    }
                    else {
                        items_checked--;
                    }
                }
                resolve('metabolitesSelectedAddAll resolved');
            })
        },
        /* Removes all metabolites from selection */
        metabolitesSelectedRemoveAll ({state, commit}) {
            return new Promise(resolve => {
                while(state.metabolites_selected.length) {
                    commit('metabolitesSelectedRemove', state.metabolites_selected[0]);
                }
                resolve('metabolitesSelectedRemoveAll resolved');
            })
        },
    },
    getters: {
        /* Gets a list of all current metabolites and their units */
        metabolitesCurrentList: (state) => (query_mode: string) => {
            if (query_mode == 'partial')
                return state.metabolites_selected.map(function(x: customObject) {
                    return [x.metabolite_name, x.unit ? x.unit : "mmol/L"]
                }).filter((met: string[]|undefined) => met !== undefined);
            else {
                return state.metabolites_all.map(function(x: customObject) {
                    for (let study of x.studies) {
                        if (state.studies_selected.includes(study)) return [x.metabolite_name, x.unit ? x.unit : "mmol/L"];
                        //if (state.studies_selected.includes(study)) return x.metabolite_name.toLowerCase()

                    }
                }).filter((met: string[]|undefined) => met !== undefined);
            }
        },
        /* Creates a temporary query for all metabolites (used e.g. in generating csv files) */
        metabolitesAllQuery (state) {
            let temporary_queries = new Map<string, customObject>();
            state.metabolites_all.forEach((metabolite: customObject) => {
                temporary_queries.set(metabolite.id, {'name': metabolite.metabolite_name, 'id': metabolite.id, 'query': metabolite.id});
            });
            return temporary_queries;
        },
        /* True if all necessary bearer tokens exist */
        loggedIn () {
            return (Cookies.get('metaboserv_jwt') && Cookies.get('metaboserv_csrf') && Cookies.get('metaboserv_username'))
        },
        getUser() {
            /* Username of logged in user */
            return Cookies.get('metaboserv_username')
        },
    }
});

export default store;