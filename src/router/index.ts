import { createRouter, createWebHistory, RouteLocation, RouteRecordRaw } from "vue-router";
import UploadView from "../views/UploadView.vue"
import DataOverView from "../views/DataOverView.vue"
import DownloadView from "../views/DownloadView.vue"
import DataGraphView from "../views/DataGraphView.vue"
import DataTableView from "../views/DataTableView.vue"
import DataRawView from "../views/DataRawView.vue"
import UserManagementView from "@/views/UserManagementView.vue";
import UserPermissionView from "@/views/UserPermissionView.vue";
import UserAccountView from "@/views/UserAccountView.vue";
import LandingPage from "@/views/LandingPage.vue";
import store from "@/store";
import { computed } from "vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "MetaboSERV",
    component: DataOverView,
    alias: ["/metaserv"],
    children: [
      {
        path: "graph",
        name: "Concentration Data - Graphs",
        component: DataGraphView,
      },
      {
        path: "table",
        name: "Concentration Data - Table",
        component: DataTableView,
      },
      {
        path: "raw",
        name: "Raw NMR Spectroscopy Data",
        component: DataRawView,
      },
    ]
  },
  {
    path: "/about",
    name: "About MetaboSERV",
    component: LandingPage,
  },
  {
    path: "/upload",
    name: "Upload Data",
    component: UploadView,
  },
  {
    path: "/download",
    name: "Download Data",
    component: DownloadView,
  },
  {
    path: "/user",
    name: "Manage Account",
    component: UserManagementView,
    children: [
      {
        path: "permissions",
        name: "Permissions",
        component: UserPermissionView,
      },
      {
        path: "account",
        name: "User account",
        component: UserAccountView,
      },
    ]
  },
];

const current_tokens = computed(() => store.state.access_tokens);

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

const hasQueryParams = (route: RouteLocation) => {
  return !!Object.keys(route.query).length;
}

router.beforeEach((to, from, next) => {
  if (!hasQueryParams(to) && hasQueryParams(from)) {
    const toWithQuery = Object.assign({}, to, {query: from.query});
    next(toWithQuery);
  } else {
    next();
  }
});

router.afterEach((to) => {
  if (store && to.query && to.query.token) {
    let access_tokens = (to.query.token as string).split(";");
    for (let token of access_tokens) store.commit('accessTokensAdd', token)
  }
})

export default router;
