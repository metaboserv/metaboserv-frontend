<template>
    <div class="pt-[2%]">
    <PlotModal :image_url="modal_url" v-if="show_modal" @close-modal="hideModal"/>
    <p v-if="query_results && results">Your query has returned <b>{{results}} results</b>. {{result_string}}</p>
    <div class="container grid grid-cols-2 mx-auto gap-8 w-[98%]">
        <PlotItem v-for="[url, itype] of plot_image_urls" :image_url="url" :plot_type="itype" :metadata="metabolite_list" class="w-full h-full" 
        @new-scatterplot="replacePlotScatter" @new-histogram="replaceHistogram" @show-modal="updateModalUrl"/>
    </div>
    <div class="w-full h-44"/>
    <div v-if="Object.keys(query_results).length && !plot_image_urls.length && !query_loading" class="h-72 mx-auto my-auto"><br><br><br><br><b>No results to show.</b><br>Consider changing the strictness of your query.</div>
    </div>
</template>

<script setup lang="ts">
import { computed, onMounted, onUnmounted, ref, watch } from 'vue';
import PlotItem from '@/components/plotting/PlotItem.vue';
import { createHistogram, createScatterPlot, generatePlotID } from "@/modules/query_flask";
import PlotModal from '@/components/plotting/PlotModal.vue';
import { useStore } from 'vuex';

/**
 * Count number of query results on mounting
 */
onMounted((): void => {
    results.value = countResults();
});

/**
 * On unmounting the view, save the last view path for seamlessness
 */
onUnmounted((): void => {
    store.commit('miscSetViewPath', "/graph");
})

const store = useStore();
const metabolite_list = computed(() => store.state.query_results_metabolites);
const plot_image_urls = computed(() => store.state.plot_image_urls);
const query_results = computed(() => store.state.query_results);
const query_nconc_string = computed(() => store.state.query_nconc_string);
const query_loading = computed(() => store.state.query_loading);
const query_phenotypes = computed(() => store.state.query_results_phenotypes);

interface customObject {
    [key: string]: any;
}

/**
 * Counts the number of results that are connected to the current user choices.
 * Used to display the number of results to the user (above the plots)
 */
const countResults = (): number => {
    let result_size = 0;
    if (!Object.keys(query_results.value).length) {
        return result_size;
    }
    let study: keyof typeof query_results.value;
    result_string.value = "(";
    for (study in query_results.value) {
        study_map.value?.set(<string>study, Object.keys(query_results.value[study]).length)
        result_size += <number>study_map.value?.get(study);
        result_string.value += (study + ": " + <number>study_map.value?.get(study)+ ", ")
    }
    result_string.value = result_string.value.slice(0, -2) + ")";
    return result_size;
}

/**
 * Replaces an old plot with a new scatterplot (e.g. after a scatterplot has been commissioned).
 * 
 * @param m1 - The metabolite to plot on one axis
 * @param m2 - The metabolite to plot on the other axis (must be different from m1)
 * @param url - The image URL of the plot to replace
 */
const replacePlotScatter = async (m1: string, m2: string, url: string): Promise<void> => {
    let current_plot_index = getPlotIndex(url);
    let meta_subset = metaboliteSubset(m1, m2)
    if (!meta_subset) return;
    let new_plot_url = await createScatterPlot(meta_subset, generatePlotID(), query_nconc_string.value, query_phenotypes.value);
    if (new_plot_url == undefined) {
        return;
    }
    console.log(new_plot_url)

    if (current_plot_index == -1 || current_plot_index == undefined) {
        store.commit('plotAdd', {'url': new_plot_url, 'plot_type': 'scatter'});
    } else {
        store.commit('plotReplace', {'url': new_plot_url, 'plot_type': 'scatter', 'plot_index': current_plot_index});
    }   
}

/**
 * Replaces an old plot with a new histogram.
 * 
 * @param m - The metabolite to plot a histogram for
 * @param url - The image URL of the plot to replace
 * @param bins - The number of bins for the histogram (optional)
 */
const replaceHistogram = async (m: string, url: string, histstyle: string, bins?: number): Promise<void> => {
    let current_plot_index = getPlotIndex(url);

    let meta_subset = metaboliteSubset(m);
    if (!meta_subset) return;

    let new_plot_url: string|void = undefined;
    if (bins) {
        new_plot_url = await createHistogram(meta_subset, generatePlotID(), query_nconc_string.value, histstyle, bins, query_phenotypes.value);
    } else {
        new_plot_url = await createHistogram(meta_subset, generatePlotID(), query_nconc_string.value, histstyle, undefined, query_phenotypes.value);
    }
    if (new_plot_url == undefined) {
        return;
    }

    if (current_plot_index == -1 || current_plot_index == undefined) {
        store.commit('plotAdd', {'url': new_plot_url, 'plot_type': 'hist'});
    } else {
        store.commit('plotReplace', {'url': new_plot_url, 'plot_type': 'hist', 'plot_index': current_plot_index});
    }  
}

/**
 * Returns the index of the plot in the store's image list.
 * 
 * @remarks Returns -1 if the image URL is not found.
 * 
 * @param url - URL of the plot to search for
 * @returns Plot index of a plot in the store
 */
const getPlotIndex = (url: string): number => {
    let current_plot_index:number = -1;
    for (let item of plot_image_urls.value) {
        if (item[0] == url) {
            current_plot_index = plot_image_urls.value.indexOf(item);
        }
    }
    return current_plot_index;
}

/**
 * Updates the image of the plot modal.
 * Also shows the modal (as it is only called if the modal is clicked).
 * 
 * @param url - The new URL for the modal
 */
const updateModalUrl = (url: string): void => {
    modal_url.value = url;
    show_modal.value = true;
}

/**
 * Hides the plot modal.
 */
const hideModal = (): void => {
    show_modal.value = false;
}

/**
 * Takes a subset from the result set only containing the metabolite(s) in question
 * Also automatically saves phenotype values.
 * 
 * @param m1 - Metabolite to include results for
 * @param m2 - Second metabolite to include results for (optional)
 */
const metaboliteSubset = (m1: string, m2?: string): customObject => {
    if (!Object.keys(query_results.value).length) {
        return {};
    }
    let result_set: customObject = {};
    for (let study in query_results.value) {
        result_set[study] = {};
        for(let source in query_results.value[study]) {
            for(let metabolite in query_results.value[study][source]) {
                if (metabolite.toLowerCase() == m1.toLowerCase() || (m2 && metabolite.toLowerCase() == m2.toLowerCase())) {
                    if (!result_set[study][source]) result_set[study][source] = {};
                    result_set[study][source][metabolite] = query_results.value[study][source][metabolite];
                }
                if (query_phenotypes.value.includes(metabolite)) {
                    if (!result_set[study][source]) result_set[study][source] = {};
                    result_set[study][source][metabolite] = query_results.value[study][source][metabolite];
                }
            }
        }
    }
    return result_set;
}

const results=ref<number>(0);
const result_string=ref<string>("");
const study_map=ref<Map<string, number>>(new Map<string, number>());
const show_modal=ref(false);
const modal_url = ref<string>("");

/**
 * If query results change, update result count and result string
 */
watch(query_results.value, (): void => {
    if (Object.keys(query_results.value).length) {
        results.value = countResults();
    }
});
</script>