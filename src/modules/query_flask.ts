/**
 * Currently using the database/microservices running on MTB public vm
 */
//export const api_link: string = "http://mtbvmpub:9201/metaboserv-api/v1/";
export const api_link: string = "http://localhost:9201/metaboserv-api/v1/";
//export const api_link: string = "http://mtb.bioinf.med.uni-goettingen.de/metaboserv-api/v1/";
let cached_metabolite_data: customObject = {};

import store from '@/store';
import Cookies from 'js-cookie';

interface customObject {
  [key: string]: any
}

/**
 * GETs data on the contained studies from flask microservice.
 * @returns JS Object
 */
export const getStudyList = async (): Promise<any> => {
  let result = await fetch(`${api_link}getStudyList`, {
      method: 'get',
      headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body
}

/**
 * GETs metadata for a study.
 * 
 * @param study_id - The ID of the study in question
 * @returns JS Object
 */
export const getStudyData = async (study_id: string): Promise<customObject> => {
  let result = await fetch(`${api_link}getStudy?` + new URLSearchParams({
    'study': study_id,
  }), {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * Updates metadata for a specific study.
 * 
 * @param study_metadata - Metadata packed into an object
 * @returns Response object
 */
export const updateStudyMetadata = async(study: string, study_metadata: customObject) => {
  let response = await fetch(`${api_link}updateStudyMetadata`, {
    method: 'post',
    headers: generateHeaders('application/json', 'application/json'),
    body: JSON.stringify({
      study: study,
      metadata: study_metadata,
    }),
  })
  return response;
}

/**
 * GETs user data (permissions, name, email etc.).
 * 
 * @param username - The ID of the user in question
 * @returns JS Object
 */
export const getUserData = async (username: string): Promise<customObject> => {
  let result = await fetch(`${api_link}getUser?` + new URLSearchParams({
    'username': username,
  }), {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * GETs data on the metabolites contained in the database.
 * 
 * @remarks Also pre-sorts the metabolites into lipids and non lipids + alphabetical order
 * 
 * @returns JS Object
 */
export const getMetabolites = async (): Promise<customObject[]> => {
  let result = await fetch(`${api_link}getMetaboliteList`, {
    method: 'get',
    headers: generateHeaders('application/json'),
  });
  cached_metabolite_data = await result.json();
  cached_metabolite_data = cached_metabolite_data.body;

  const sorted_metabolite_data = Object.keys(cached_metabolite_data)
    .sort(function compareFn (a: string, b: string) {
      if (cached_metabolite_data[a]["metabolite_cat"] == "lipid" && cached_metabolite_data[b]["metabolite_cat"] == "non-lipid") {
        return 1;
      }
      else if (cached_metabolite_data[a]["metabolite_cat"] == "non-lipid" && cached_metabolite_data[b]["metabolite_cat"] == "lipid") {
        return -1;
      }
      else {
        return cached_metabolite_data[a]["metabolite_name"].localeCompare(cached_metabolite_data[b]["metabolite_name"]);
      }
    })
    .reduce((accumulator: Array<customObject>, key: string) => {
      accumulator.push(cached_metabolite_data[key]);
      return accumulator;
    }, []);
  return sorted_metabolite_data;
}

/**
 * GETs data on the metabolites contained in the database.
 * Only returns metabolites for one particular study.
 * Also returns coverage values.
 * 
 * @remarks Also pre-sorts the metabolites into lipids and non lipids + alphabetical order
 * 
 * @returns JS Object
 */
export const getMetabolitesForStudy = async (study: string): Promise<customObject[]> => {
  let result = await fetch(`${api_link}getMetaboliteListForStudy?` + new URLSearchParams({
    'study': study,
  }), {
    method: 'get',
    headers: generateHeaders('application/json'),
  });
  cached_metabolite_data = await result.json();
  cached_metabolite_data = cached_metabolite_data.body;

  const sorted_metabolite_data = Object.keys(cached_metabolite_data)
    .sort(function compareFn (a: string, b: string) {
      if (cached_metabolite_data[a]["metabolite_cat"] == "lipid" && cached_metabolite_data[b]["metabolite_cat"] == "non-lipid") {
        return 1;
      }
      else if (cached_metabolite_data[a]["metabolite_cat"] == "non-lipid" && cached_metabolite_data[b]["metabolite_cat"] == "lipid") {
        return -1;
      }
      else if (cached_metabolite_data[a]["coverage"] < cached_metabolite_data[b]["coverage"]) {
        return 1;
      }
      else if (cached_metabolite_data[a]["coverage"] > cached_metabolite_data[b]["coverage"]) {
        return -1;
      }
      else {
        return cached_metabolite_data[a]["metabolite_name"].localeCompare(cached_metabolite_data[b]["metabolite_name"]);
      }
    })
    .reduce((accumulator: Array<customObject>, key: string) => {
      accumulator.push(cached_metabolite_data[key]);
      return accumulator;
    }, []);
  return sorted_metabolite_data;
}

/**
 * Retrieves a list of all sources in a particular study.
 * 
 * @param study - ID of the study
 * @returns A list of all source entries contained in a study.
 */
export const getSourcesInStudy = async (study: string): Promise<customObject> => {
  let result = await fetch(`${api_link}getSourcesInStudy?` + new URLSearchParams({
    'study': study,
  }), {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * Retrieves a list of all users with permissions for a study.
 * 
 * @param study - ID of the study
 * @returns A list of all users with permissions for the study
 */
export const getPermittedUsers = async (study: string): Promise<customObject> => {
  let result = await fetch(`${api_link}getPermittedUsers?` + new URLSearchParams({
    'study': study,
  }), {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * Issues a new permission token for a specific study.
 * 
 * @param study - The study in question (can be a semicolon-separated string with several studies)
 * @param perm_type - Viewing or managing permissions
 * @param expiration - Expiration date (optional), given in days
 * @returns Response object
 */
export const generateNewToken = async(study: string, perm_type: string, expiration: number, comment: string) => {
  const token_form: FormData = new FormData();
  token_form.append('study', study);
  token_form.append('perm_type', perm_type);
  token_form.append('expiration', expiration.toString());
  if (comment && comment != "") token_form.append('comment', comment);
  let response = await fetch(`${api_link}generateToken`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  })
  return response;
}

/**
 * Revokes a token.
 * 
 * @param token_id - The ID of the token to revoke
 * @returns Response object
 */
export const revokeExistingToken = async(token_id: string) => {
  const token_form: FormData = new FormData();
  token_form.append('id', token_id);
  let response = await fetch(`${api_link}revokeToken`, {
    method: 'post',
    headers: generateHeaders(),
    body: token_form,
  }).catch((error) => {
    console.log(error);
  })
  return response;
}

/**
 * GETs all tokens for a study issued by the user currently logged in.
 * 
 * @param study - Study to get tokens for
 * @returns JS Object
 */
export const getTokenList = async (study: string) => {
  let result = await fetch(`${api_link}getTokenList?` + new URLSearchParams({
    'study': study,
  }), {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * GETs a list of applicable phenotypes based on current JWT or auth token.
 * 
 * @param study - (optional) Studies to consider. Semicolon-separated.
 * @returns JS Object
 */
export const getPhenotypeList = async (study?: string): Promise<customObject> => {
  let link_to_fetch = `${api_link}getPhenotypeList?`;
  if (study) link_to_fetch = link_to_fetch + new URLSearchParams({'study': study})
  let result = await fetch(link_to_fetch, {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * Interface for objects holding normal concentration query parameters.
 */
export interface nconcParams {
  specimen?: string,
  age?: string,
  sex?: string,
  units?: string,
  source?: string,
}

/**
 * Retrieves normal concentration ranges for one or more metabolites.
 * Allows for further filtering options (agegroup(s), sex, biospecimen, unit(s))
 * 
 * @remarks All strings except "sex" can be semicolon-separated for bulk querying
 * 
 * @param metabolite - Metabolite(s) to retrieve normal concentrations for
 * @param nconc_params - Further query parameters to filter by
 * @returns A list of all users with permissions for the study
 */
export const getNormalConcentrationRanges = async (metabolite: string, nconc_params: nconcParams): Promise<customObject> => {
  let params = new URLSearchParams({'metabolite': metabolite});
  if (nconc_params.specimen) params.append('specimen', nconc_params.specimen);
  if (nconc_params.age) params.append('agegroup', nconc_params.age);
  if (nconc_params.sex) params.append('sex', nconc_params.sex);
  if (nconc_params.units) params.append('unit', nconc_params.units);
  if (nconc_params.source) params.append('source', nconc_params.source);
  let result = await fetch(`${api_link}getNormalConcentration?` + params, {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * Filters database sources (e.g. patients) by querying them for certain metabolites and/or their concentrations.
 * @param query_ranges - Queries for each metabolite, e.g. "glucose", "pyruvate:0.04-0.10", "alanine:oob|0.1-0.2"
 * @param studies - Studies to consider, semicolon separated
 * @param set_plot_commands - Comma separated string that dictates which plots to generate, e.g. "hist,corrmap"
 * @param set_plot_id - Plot ID to use for the plots (and later on their retrieval)
 * @param set_plot_nconc - Normal concentration values to use for the plot (min-max)
 * @param phenotypes - Optional, specifies some phenotypes to try and query data for
 * @returns JS Object
 */
export const filterByMetabolites = async (query_ranges: Map<string, customObject>, studies: Array<string>,
  set_plot_commands: string, set_plot_id: string, set_plot_nconc: string, query_mode: string, phenotypes?: string[]): Promise<any> => {
  if (!studies || !query_ranges) {
    return [{}];
  }
  let query_string = "";
  for (let metabolite of query_ranges.keys()) {
    query_string += (query_ranges.get(metabolite)!.query) + ";"
  }
  query_string = query_string.slice(0, -1) //Remove last comma
  let url_params = new URLSearchParams({
    'study': studies.join(";"),
    'metabolite': query_string,
    'plot_command': set_plot_commands,
    "plot_id": set_plot_id,
    "plot_nconc": set_plot_nconc,
    "mode": query_mode,
    });
  if (phenotypes) url_params.append("phenotype", phenotypes.join(";"));
  let result = await fetch(`${api_link}filterByMetabolites?` + url_params, {
   method: 'get',
   headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json.body;
}

/**
 * Uploads study files to the server.
 * Server-side, file types and format will be validated.
 * 
 * @param file - File object to upload
 * @returns Success/Failure Response
 */
export const uploadData = async (study: string, conc_file: File, metadata_file?: File, pheno_file?: File) => {
  const file_form: FormData = new FormData();
  file_form.append("study", study);
  file_form.append("data_file", conc_file, conc_file.name);
  if (metadata_file) file_form.append("metadata_file", metadata_file, metadata_file.name)
  if (pheno_file) file_form.append("pheno_file", pheno_file, pheno_file.name)
  const response = await fetch(`${api_link}uploadData`, {
    method: 'post',
    body: file_form,
    headers: generateHeaders(),
  })
  return response;
}

/**
 * Triggers backend to start data processing.
 * At this point the backend will have the needed files in its "pending file list".
 * 
 * @param study - Study to process files for
 * @param md - Other study metadata (authors, study type, analytical method..)
 * @returns Success/Failure Response
 */
export const processData = async (study: string, md: customObject) => {
  const response = await fetch(`${api_link}processData`, {
    method: 'post',
    body: JSON.stringify({study: study, metadata: md}),
    headers: generateHeaders('application/json', 'application/json'),
  })
  return response;
}

/**
 * Checks if a study title or user name exists
 * 
 * @param id - Identifier
 * @param id_type - Either 'study' or 'user', defaults to 'study'
 * @returns True/False
 */
export const checkIDExists = async (id: string, id_type?: string) => {
  let result = await fetch(`${api_link}checkName?` + new URLSearchParams({
    'id': id,
    'type': id_type ? id_type : 'study',
  }), {
    method: 'get',
    headers: generateHeaders('application/json')
  });
  let result_json = await result.json();
  return result_json;
}

/**
 * Retrieves a plot from the flask microservice.
 * 
 * @param id - Plot ID and command (ID:command) used for retrieval
 * @returns - Image URL of the plot
 */
export const retrievePlot = async (id: string): Promise<string|void> => {
  let result_url = await fetch(`${api_link}getPlot?` + new URLSearchParams({
    'plot_id': id,
  }), {
    method: 'get',
    headers: generateHeaders('image/png')
  })
  .then(res => {
    if (res.status == 200) {
      return res.blob()}
    else {
      throw new Error("No plot found for ID.")
    }})
  .then(blob => {
    return URL.createObjectURL(blob);
  }).catch(error => {
    return;
  });
  return result_url
}

/**
 * Uses flask and R microservices to read bruker formatted spectral data belonging to the GCKD Baseline study.
 * Returns the x and y coordinates in JS Object notation.
 * 
 * @param source_id - Source ID to parse the spectrum for
 * @returns x and y coordinates as JS Object
 */
export const getRawSpectrum_Bruker = async (source_id: string, study: string): Promise<any> => {
  let result_object = await fetch(`${api_link}getRawSpectrum_Bruker?` + new URLSearchParams({
    'source': source_id,
    'study': study,
  }), {
    method: 'get',
    headers: generateHeaders('application/json'),
  })
  .then(res => {
    if (res.status == 200) {
      return res.json()}
    else {
      throw new Error("Error encountered while retrieving raw spectrum.")
    }}).catch(error => {
      return;
    }
  );
  return result_object.body
}

/**
 * Tells the flask microservice to generate a scatterplot using Matplotlib.
 * 
 * @param result_subset - Data subset to plot
 * @param id - Plot ID to use (contains plot type)
 * @param nconc - Normal concentration data to plot
 * @returns URL of the resulting plot
 */
export const createScatterPlot = async (result_subset: Object, id: string, nconc: string, phenotypes?: string[]): Promise<string|void> => {
  let plot_form = new FormData();
  plot_form.append('plot_id', id);
  plot_form.append('data', JSON.stringify(result_subset));
  plot_form.append('plot_nconc', nconc)
  if (phenotypes) plot_form.append('phenotype', phenotypes.join(";"));
  let plot_url = await fetch(`${api_link}createScatterPlot`, {
    method: 'post',
    body: plot_form,
  }).then(res => {
    switch(res.status) {
      case 200:
        return retrievePlot(id+"_scatter");
      default:
        return;
    }
  })
  if (plot_url) {
    return plot_url;
  }
}

/**
 * Tells the flask microservice to generate a histogram using Matplotlib.
 * 
 * @param result_subset - Data subset to plot
 * @param id - Plot ID to use (contains plot type)
 * @param nconc - Normal concentration data to plot
 * @param bins - Number of bins for histogram (optional)
 * @returns URL of the resulting plot
 */
export const createHistogram = async (result_subset: Object, id: string, nconc: string, histstyle: string, bins?: number, phenotypes?: string[]): Promise<string|void> => {
  let plot_form = new FormData();
  plot_form.append('plot_id', id);
  plot_form.append('data', JSON.stringify(result_subset));
  plot_form.append('plot_nconc', nconc);
  plot_form.append('histstyle', histstyle);
  if (bins) {
    plot_form.append('plot_bins', bins.toString());
  }
  if (phenotypes) plot_form.append('phenotype', phenotypes.join(";"));
  let plot_url = await fetch(`${api_link}createHistPlot`, {
    method: 'post',
    body: plot_form,
  }).then(res => {
    switch(res.status) {
      case 200:
        return retrievePlot(id+"_hist");
      default:
        return;
    }
  })
  if (plot_url) {
    return plot_url;
  }
}

/**
 * Generates a plot ID based on current timestamp.
 * 
 * @returns Plot ID string
 */
export const generatePlotID = (): string => {
  const current_date = new Date();
  return (current_date.getMonth()+"_"+current_date.getDay()+"_"+current_date.getHours()+"_"+current_date.getMinutes()+"_"+current_date.getSeconds())
}

/**
 * Retrieves a source file from the flask microservice.
 * 
 * @param filename - Name of the file to get
 * @param study - Name of the study to get the file for
 * @return File object (?)
 */
export const getSourceFile = async (filename: string, study: string): Promise<Blob> => {
  let result = await fetch(`${api_link}getSourceFile?` + new URLSearchParams({
    'file': filename,
    'study': study,
  }), {
    method: 'get',
    headers: generateHeaders('application/octet-stream')
  });
  return result.blob();
}

/**
 * Queries the flask microservice to generate a tar archive containing several source files.
 * 
 * @param filename_string - Semicolon-separated filenames
 * @param study - Name of the study to get 2 or more files for
 * @returns TAR Archive as blob
 */
export const getSourceArchive = async (filename_string: string, study: string): Promise<Blob> => {
  let result = await fetch(`${api_link}getSourceArchive?` + new URLSearchParams({
    'file': filename_string,
    'study': study,
  }), {
    method: 'get',
    headers: generateHeaders('application/x-tar')
  });
  return result.blob();
}

/**
 * Queries the flask microservice to generate a .csv file with user defined parameters.
 * 
 * @param params - User defines parameters (filename, separator, NA method)
 * @returns CSV file as blob
 */
export const getCSV = async(params: customObject, data: customObject, metabolites: string[]): Promise<Blob | undefined> => {
  let csv_form = new FormData();
  console.log(params)
  console.log(data)
  console.log(metabolites)
  csv_form.append('data', JSON.stringify(data));
  csv_form.append('na', params.na);
  csv_form.append('sep', params.sep);
  csv_form.append('filename', params.filename);
  csv_form.append('metabolites', metabolites.join(';'))
  let csv = await fetch(`${api_link}generateCSV`, {
    method: 'post',
    body: csv_form,
  }).then( async (res) => {
    switch(res.status) {
      case 200:
        let response_json = await res.json();
        console.log(response_json)
        let csv_id = response_json.body.id;
        let result = await fetch(`${api_link}retrieveCSV?` + new URLSearchParams({
          'id': csv_id,
        }), {
          method: 'get',
          headers: generateHeaders('application/octet-stream')
        })
        return result;
      default:
        return;
    }
  })
  if (csv) {
    return csv!.blob();
  }
}

/**
 * Logs in a user if username and password match the database.
 * 
 * @param username - Username provided by the user
 * @param password - Password provided by the user (encrypted)
 * @returns Login message (Success, failure)
 */
export const login = async(username: string, password: string) => {
  let response = await fetch(`${api_link}login`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify({username: username, password: password, cookie: 'true'}),
  }).then( async (res) => {
    if (res.status == 401) return {'msg': 'Login failed!'}
    return await res.json()
  })
  return response;
}

/**
 * Gets a value for a cookie (basically key-value-pairs) in the browser.
 * Used for JWT cookies.
 * 
 * @param name - Cookie name
 * @returns - Cookie value
 */
export const getCookie = (name: string) => {
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${name}=`);
  if (parts && parts.length === 2) return parts.pop()!.split(';').shift();
}

/**
 * Generates headers for communication with the flask backend.
 * Considers JWT tokens (set through cookies) or general access tokens (provided through URL)
 * 
 * @param content_type - Content Type of the HTTP body
 * @param accept - Accepted content type for the header
 * @returns Headers with (optional) content-type, (optional) accept and auth token
 */
export const generateHeaders = (content_type?: string, accept?: string) => {
  let header_obj = {}
  if (content_type) header_obj['Content-Type'] = content_type;
  if (accept) header_obj['Accept'] = accept;
  if (getCookie('metaboserv_jwt')) header_obj['Authorization'] = `Bearer ${Cookies.get('metaboserv_jwt')}`;
  if (getCookie('metaboserv_csrf')) header_obj['X-CSRF-TOKEN'] = Cookies.get('metaboserv_csrf');
  if (store) {
    let access_tokens = store.state.access_tokens;
    header_obj['PERM-TOKEN'] = access_tokens.join(";");
  }
  return header_obj;
}

/**
 * Test method for checking if the JWT tokens are working.
 * 
 * @returns Response detailing permission check success or failure
 */
export const testCookie = async () => {
  let response = await fetch(`${api_link}whoami`, {
    method: 'get',
    credentials: 'same-origin',
    headers: generateHeaders(),
  });
  return response;
}

/**
 * Registers a new user account.
 * 
 * @param username - Username
 * @param password - Password (encrypted)
 * @param email - E-Mail Address
 * @param institution - Institution
 * @returns - Message (success, failure)
 */
export const register = async(username: string, password: string, email: string, institution: string) => {
  let response = await fetch(`${api_link}register`, {
    method: 'post',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    },
    body: JSON.stringify({username: username, password: password, email: email, institution: institution}),
  }).then( async (res) => {
    if (res.status == 401) return {'msg': 'User with that name exists already!'}
    return await res.json()
  })
  return response;
}

/**
 * Adds a contributor to a study.
 * 
 * @param study - The study in question
 * @param username - Name of the user to add as contributor
 * @param manage - Sets true or false for managing permissions
 * @returns Response object
 */
export const addContributor = async(study: string, username: string, manage: boolean) => {
  const contrib_form: FormData = new FormData();
  contrib_form.append('study', study);
  contrib_form.append('username', username);
  contrib_form.append('manage', manage.toString());
  let response = await fetch(`${api_link}addContributor`, {
    method: 'post',
    headers: generateHeaders(),
    body: contrib_form,
  }).then(async (res) => {
    if (res.status == 200) return {'msg': `Successfully added contributor ${username} to study ${study}.`}
    else if (res.status == 401) return {'msg': `"Sorry, you lack the required permissions to add contributors to the study ${study}."`}
    else if (res.status == 409) return {'msg': `User ${username} is already a contributor for the study ${study}.` }
    else if (res.status == 404) return {'msg': `User ${username} was not found.`}
    else return {'msg': "Sorry, something went wrong. Please refresh the page and try again."}
  })
  
  return response;
}

/**
 * Removes a contributor (i.e. their permissions) from a study.
 * 
 * @param study - The study in question
 * @param username - Name of the user to add as contributor
 * @returns Response object
 */
export const removeContributor = async(study: string, username: string) => {
  const contrib_form: FormData = new FormData();
  contrib_form.append('study', study);
  contrib_form.append('username', username);
  let response = await fetch(`${api_link}removeContributor`, {
    method: 'post',
    headers: generateHeaders(),
    body: contrib_form,
  }).then(async (res) => {
    if (res.status == 200) return {'msg': `Successfully removed contributor ${username} from study ${study}.`}
    else if (res.status == 401) return {'msg': `"Sorry, you lack the required permissions to revoke permissions for the study ${study}."`}
    else if (res.status == 403) return {'msg': `You can not revoke permissions for the uploader of the study ${study} (or for yourself).`}
    else if (res.status == 404) return {'msg': `User ${username} was not found.`}
    else return {'msg': "Sorry, something went wrong. Please refresh the page and try again."}
  })
  return response;
}

/**
 * Updates existing managing permissions for a contributor on a study.
 * 
 * @param study - The study in question
 * @param username - Name of the user to add as contributor
 * @param manage - Sets true or false for managing permissions
 * @returns Response object
 */
export const updateContributor = async(study: string, username: string, manage: boolean) => {
  const contrib_form: FormData = new FormData();
  contrib_form.append('study', study);
  contrib_form.append('username', username);
  contrib_form.append('manage', manage.toString());
  let response = await fetch(`${api_link}updateContributor`, {
    method: 'post',
    headers: generateHeaders(),
    body: contrib_form,
  }).then(async (res) => {
    if (res.status == 200) return {'msg': `Successfully updated management permissions for user ${username} on study ${study}.`}
    else if (res.status == 401) return {'msg': `"Sorry, you lack the required permissions to update permissions for the study ${study}."`}
    else if (res.status == 403) return {'msg': `You can not update management permissions for the uploader of the study ${study} (or for yourself).`}
    else if (res.status == 404) return {'msg': `User ${username} was not found.`}
    else return {'msg': "Sorry, something went wrong. Please refresh the page and try again."}
  })
  return response;
}




