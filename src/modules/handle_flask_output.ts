import { getStudyList, getMetabolites, retrievePlot, getStudyData, filterByMetabolites } from "@/modules/query_flask";
import { computed } from 'vue';
import store from '@/store/index';

const studies_selected = computed(() => store.state.studies_selected);
const concentrations_normal = computed(() => store.state.concentrations_normal);
const studies_all = computed(() => store.state.studies_all);
const selected_phenotypes = computed(() => store.state.selected_phenotypes);

interface customObject {
    [key: string]: any;
}

/**
 * Queries the flask microservice for all studies contained in the database.
 */
export const flaskGetStudies = async (): Promise<void> => {
    let call_content = await getStudyList();
    store.commit('studyClear');
    Object.keys(call_content).forEach(function(key: string) {
        if (!studies_all.value.includes(call_content[key].study_id)) {
            store.commit('studyKnownAdd', call_content[key].study_id);
        }    
    })
    store.commit('studySort');
}

/**
 * Queries the flask microservice for all metabolites contained in the database.
 * 
 * @remarks Also updates sorting order and sorts the metabolite list.
 */
export const flaskGetMetabolites = async (): Promise<void> => {
    store.commit('metabolitesClear');
    let call_content = await getMetabolites();
    store.dispatch('metabolitesLoad', call_content);
    //Dirty workaround for IDs that are missing in backend (should not happen, but always good to have a failsafe):
    call_content.forEach((key) => {
        if (!key.id) {
            key.id = key.metabolite_name.toLowerCase().replace(" ", "_");
        }
    })
    store.dispatch('metabolitesUpdateAll').then(() => store.dispatch('metabolitesSort'));
}

/**
 * Main data filtering method. Queries flask microservice to retrieve a subset of data entries.
 * 
 * @remarks Will also cause the microservice to generate plots under given ID and command.
 * 
 * @param query_ranges - Ranges containing the allowed min. and max. concentration values for each query metabolite (e.g. glucose:0-0.05)
 * @param plot_commands - String that denotes which plot(s) to generate
 * @param plot_id  - String that will be used as identifier for generated plots (id:command)
 * @param process_mode - If "temp", saves results to a temporary object. For download queries
 * @param query_mode - Full for complete queries (always returns all metabolites), partial otherwise
 */
export const flaskFilterByMetabolites = async (query_ranges: Map<string, customObject>, 
    plot_commands: string, plot_id: string, process_mode: string, query_mode: string, phenotypes?: string[]): Promise<void> =>  {
    let nconc_string = generateNconcString();
    let call_content= await filterByMetabolites(query_ranges, studies_selected.value, plot_commands, plot_id, nconc_string, query_mode, phenotypes);
    if (process_mode === "temp") {
        store.commit('queryTempSet', call_content);
    } else {
        store.commit('queryPhenotypesSet', selected_phenotypes.value);
        store.commit('queryResultsSet', call_content);
        store.commit('queryNconcStringSet', nconc_string);
    }
}

/**
 * Retrieves an image for a plot from the flask microservice.
 * 
 * @remarks The identifier here contains both the plot id and the plot command!
 *          Both are required to retrieve the correct plot.
 * 
 * @param plot_id - Identifier to retrieve the plot.
 * @returns An URL for the plot to be displayed.
 */
export async function flaskRetrievePlot(plot_id: string): Promise<string|void> {
    let plot_url = await retrievePlot(plot_id).then(res => {
        return res;
    });
    return plot_url;
}

/**
 * Generates a query string ("nconc string") for queries that include normal concentration values.
 * 
 * @returns Normal concentration query string (e.g. somethingine|3.1-3.3,otherine|0.01-0.04)
 */
export const generateNconcString = (): string => {
    if(!concentrations_normal.value) {
        return "";
    } else {
        let res: string[] = [];
        concentrations_normal.value.forEach((cval: [number, number], metabolite: string) => {
            res.push(metabolite + "|" + cval[0] + "-" + cval[1]);
        })
        return res.join(";");
    }
}

/**
 * Gets metadata for a study and stores it in a map.
 * 
 * @param study_id - Identifier for the study
 * @returns Object with study metadata
 */
export const flaskGetStudyMetadata = async (study_id: string): Promise<customObject> =>  {
    let call_content = await getStudyData(study_id);
    return call_content;
}

